package com.jlngls.funcoes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        exibirMensagem("John")
    }
    fun exibirMensagem(nome: String){
        println("exibir")
        var resultado = somar(3,4)
        print(resultado)
    }


    // funcao com retorno

    fun somar (num1: Int, num2: Int): Int {
        var total = num1 + num2
        return total
    }
}